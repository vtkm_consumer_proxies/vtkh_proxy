//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/ImplicitFunctionHandle.h>
#include <vtkm/rendering/Camera.h>

// clip a DataSet with a provided implicit function
vtkm::cont::DataSet
clip_with_implicit_func(const vtkm::cont::DataSet &input,
                        const vtkm::cont::ImplicitFunctionHandle &func);

vtkm::cont::DataSet clip_with_sphere(const vtkm::cont::DataSet &input,
                                     vtkm::Vec<vtkm::FloatDefault, 3> center,
                                     vtkm::FloatDefault radius);

vtkm::cont::DataSet clip_with_plane(const vtkm::cont::DataSet &input,
                                    vtkm::Vec<vtkm::FloatDefault, 3> origin,
                                    vtkm::Vec<vtkm::FloatDefault, 3> normal);

vtkm::cont::DataSet clean_grid(vtkm::cont::DataSet &input);

vtkm::cont::DataSet
contour_with_isovalues(const vtkm::cont::DataSet &input,
                       const std::string &field_name,
                       const std::vector<vtkm::Float64> &values);
