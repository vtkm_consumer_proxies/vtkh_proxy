//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/filter/ClipWithImplicitFunction.h>

vtkm::cont::DataSet
clip_with_implicit_func(const vtkm::cont::DataSet &input,
                        const vtkm::cont::ImplicitFunctionHandle &func) {
  vtkm::filter::ClipWithImplicitFunction clipper;
  clipper.SetImplicitFunction(func);
  return clipper.Execute(input);
}

vtkm::cont::DataSet clip_with_sphere(const vtkm::cont::DataSet &input,
                                     vtkm::Vec<vtkm::FloatDefault, 3> center,
                                     vtkm::FloatDefault radius) {
  auto func =
      vtkm::cont::make_ImplicitFunctionHandle(vtkm::Sphere(center, radius));
  return clip_with_implicit_func(input, func);
}

vtkm::cont::DataSet clip_with_plane(const vtkm::cont::DataSet &input,
                                    vtkm::Vec<vtkm::FloatDefault, 3> origin,
                                    vtkm::Vec<vtkm::FloatDefault, 3> normal) {
  auto func =
      vtkm::cont::make_ImplicitFunctionHandle(vtkm::Plane(origin, normal));
  return clip_with_implicit_func(input, func);
}
