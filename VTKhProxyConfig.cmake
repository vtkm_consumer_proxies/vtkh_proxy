##============================================================================
##  Copyright (c) Kitware, Inc.
##  All rights reserved.
##  See LICENSE.txt for details.
##
##  This software is distributed WITHOUT ANY WARRANTY; without even
##  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##  PURPOSE.  See the above copyright notice for more information.
##============================================================================

if (CMAKE_VERSION VERSION_LESS "3.8")
  message(FATAL_ERROR "VTKh Proxy requires CMake 3.8+")
endif()

set(VTKm_DIR @VTKm_DIR@)
find_package(VTKm REQUIRED QUIET)
include(${CMAKE_CURRENT_LIST_DIR}/VTKhProxyTargets.cmake)
