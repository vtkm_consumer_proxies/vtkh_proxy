//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/filter/Contour.h>

vtkm::cont::DataSet
contour_with_isovalues(const vtkm::cont::DataSet &input,
                       const std::string &field_name,
                       const std::vector<vtkm::Float64> &values) {
  vtkm::filter::Contour marcher;
  marcher.SetIsoValues(values);
  marcher.SetMergeDuplicatePoints(true);
  marcher.SetActiveField(field_name);

  return marcher.Execute(input);
}
