//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
#include <vtkm/filter/CleanGrid.h>

vtkm::cont::DataSet
clean_grid(vtkm::cont::DataSet &input)
{
    vtkm::filter::CleanGrid cleaner;
    auto output = cleaner.Execute(input);
    return output;
}
