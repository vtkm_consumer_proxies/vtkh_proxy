//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/Bounds.h>

#include <vtkm/cont/ColorTable.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/rendering/Actor.h>
#include <vtkm/rendering/Camera.h>
#include <vtkm/rendering/CanvasRayTracer.h>
#include <vtkm/rendering/MapperRayTracer.h>
#include <vtkm/rendering/Scene.h>
#include <vtkm/rendering/View3D.h>

bool render_data(const vtkm::cont::DataSet &input, const std::string &field_name,
                 const vtkm::Bounds& scene_bounds, const vtkm::Id2 &width_height,
                 vtkm::rendering::Camera &camera,
                 const std::string& image_name) {
  vtkm::cont::ColorTable colorTable("inferno");
  vtkm::rendering::Scene scene;
  scene.AddActor(vtkm::rendering::Actor(input.GetCellSet(),
                                        input.GetCoordinateSystem(),
                                        input.GetField(field_name), colorTable));

  // Create a scene for rendering the input data
  vtkm::rendering::Color bg(0.2f, 0.2f, 0.2f, 1.0f);
  vtkm::rendering::CanvasRayTracer canvas(width_height[0], width_height[1]);
  vtkm::rendering::MapperRayTracer mapper;

  // Create a view and use it to render the input data using OS Mesa
  camera.ResetToBounds(scene_bounds);
  vtkm::rendering::View3D view(scene, mapper, canvas, camera, bg);
  view.Initialize();
  view.Paint();
  view.SaveAs(image_name);
  return true;
}
