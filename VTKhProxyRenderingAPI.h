//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <vtkm/cont/DataSet.h>
#include <vtkm/rendering/Camera.h>

bool render_data(const vtkm::cont::DataSet &input,
                 const std::string &field_name,
                 const vtkm::Bounds& scene_bounds,
                 const vtkm::Id2& width_height,
                 vtkm::rendering::Camera& camera,
                 const std::string& image_name);
